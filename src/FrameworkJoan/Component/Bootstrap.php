<?php
namespace Mpwarfwk\Component;

use Mpwarfwk\Component\Request\Request;
use Mpwarfwk\Component\Routing\Route;
use Mpwarfwk\Component\Routing\Routing;
use Mpwarfwk\Component\Session\Session;

class Bootstrap
{
    public function __construct()
    {
        //echo 'Bootstrap';
    }

    public function execute()
    {
        $request = new Request(new Session());

        $routing = new Routing();


        $route = $routing->getRoute($request);

        //echo "$route: ".var_dump($route);
        //echo "$request: ".$request;

        $response = $this->executeController($route, $request);

        return $response;
    }

    private function executeController(Route $route, Request $request)
    {
        $controller_class = $route->getRouteClass();

        return call_user_func_array(
            array(
                new $controller_class($request),
                $route->getRouteAction()
            ),
            array($request)
        );
    }
}
